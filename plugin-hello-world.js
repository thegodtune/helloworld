const afterAllInstalled = project => {
    console.log("This is the message after package is installed\nThis is made possible through the validateProject hook");
}

const validateProject = project => {
    console.log("This is the message before package is installed\nThis is made possible through the validateProject hook");
}
//comment for the culture
//another
//new comment 

module.exports = {
    name: `plugin-hello-world`,
    factory: require => {
        
        const {Command} = require('clipanion');
        class HelloDebian extends Command {
            async execute (){
                this.context.stdout.write(`Hi there, first yarn package\n`)
            }
        }
        HelloDebian.addPath(`hello`);
        return {
            commands: [HelloDebian],
            hooks: {afterAllInstalled,  validateProject}
        }
    }
    
}